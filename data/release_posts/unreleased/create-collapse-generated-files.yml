features:
  secondary:
  - name: "Automatically collapse generated files in merge requests"
    available_in: [core, premium, ultimate]  # Include all supported tiers
    documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/changes.html#collapse-generated-files'
    image_url: '/images/unreleased/create-collapse-generated-files.png'
    reporter: phikai
    stage: create
    categories:
    - 'Code Review Workflow'
    epic_url:
    - 'https://gitlab.com/groups/gitlab-org/-/epics/9903'
    description: |
      Merge requests can contain changes from users and from automated processes or compilers. Files like `package-lock.json`, `Gopkg.lock`, and minified `js` and `css` files increase the number of files shown in a merge request review, and distract reviewers from the human-generated changes. Merge requests now display these files collapsed by default, to help:

      - Focus reviewer attention on important changes, but enabling a full review if desired.
      - Reduce the amount of data needed to load the merge request, which might help larger merge requests perform better.

      For examples of the file types that are collapsed by default, see the [documentation](https://docs.gitlab.com/ee/user/project/merge_requests/changes.html#collapse-generated-files). To collapse more files and file types in the merge request, specify them as `gitlab-generated` in your project's `.gitattributes` file.

      Feedback on this change can be left in [issue #438727](https://gitlab.com/gitlab-org/gitlab/-/issues/438727).
