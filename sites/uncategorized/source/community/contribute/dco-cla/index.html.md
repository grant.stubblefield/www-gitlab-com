---
layout: markdown_page
title: "Contributions: the GitLab DCO & CLA"
description: "Learn more about the DCO or CLA which apply to contributions to GitLab."
canonical_path: "/community/contribute/dco-cla/"
---

## All contributions subject to the DCO or the CLA

All contributions to GitLab are subject to the [Developer Certificate of Origin](https://docs.gitlab.com/ee/legal/developer_certificate_of_origin.html#developer-certificate-of-origin-version-11) (DCO), or the [Corporate](https://docs.gitlab.com/ee/legal/corporate_contributor_license_agreement.html) or [Individual](https://docs.gitlab.com/ee/legal/individual_contributor_license_agreement.html) Contributor License Agreement (CLA), depending on where you're contributing and on whose behalf.

#### Which agreement applies and when?

- Individual and Corporate contributions to MIT-licensed code are subject to the [DCO](https://docs.gitlab.com/ee/legal/developer_certificate_of_origin.html#developer-certificate-of-origin-version-11).
- Individual contributions to code in the gitlab-org/gitlab/ee directory are subject to the [Individual CLA](https://docs.gitlab.com/ee/legal/individual_contributor_license_agreement.html).
- Corporate contributions to code in the gitlab-org/gitlab/ee directory are subject to the [Corporate CLA](https://docs.gitlab.com/ee/legal/corporate_contributor_license_agreement.html).

Learn more about our [commitment to open source stewardship](/company/stewardship/).

#### AI-generated contributions

You can make AI-generated contributions to GitLab, including documentation and code, provided you follow these guidelines.

1. Review and keep GitLab’s [AI Ethics Principles for Product Development](https://handbook.gitlab.com/handbook/legal/ethics-compliance-program/ai-ethics-principles/) in mind.
2. Validate the output with first-hand information or a third-party trustworthy source.
3. Remove anything you suspect might be personal data.
4. Disclose the fact that the contribution contains AI-generated content.
5. Ensure that the terms of the generative AI tools do not prohibit or otherwise restrict use of the tool’s output in a way that is inconsistent with the [GitLab DCO or CLA](https://about.gitlab.com/community/contribute/dco-cla/#all-contributions-subject-to-the-dco-or-the-cla) (whichever is applicable to your contribution).
6. If your contribution contains pre-existing, copyrighted third-party content:
   - Confirm that you have permission from the owners of the content to use and modify the content and contribute it under the [GitLab DCO or CLA](https://about.gitlab.com/community/contribute/dco-cla/#all-contributions-subject-to-the-dco-or-the-cla) (whichever is applicable to your contribution); and
   - Include notice and attribution of these third-party rights (including information on the applicable license terms) in your contribution.

The GitLab mission is to make it so that everyone can contribute. We believe and envision that every company will need to become an AI company to stay competitive. Additionally, we believe that AI functionality will lead to major improvements in GitLab and our customers’ workflows. AI-generated contributions can assist us in achieving our mission and vision, and are welcome, provided that these contributions are in line with these guidelines.

These guidelines may change based on what we learn over time – we will continue to review and iterate as necessary to reflect the best practices in responsible development.

## Accepting the DCO or the CLA

By contributing to GitLab, you are deemed to have accepted the agreement (DCO, or Corporate or Individual CLA) applicable to your contribution.

#### Need a Corporate CLA covering all contributors on behalf of your organization?

To enter into an overarching Corporate CLA that covers all contributions made on behalf of a corporate contributor, reach out to `cla_managers@gitlab.com`.

## Frequently asked questions

#### Are my contributions to GitLab attributed to me?

A record of contributions to GitLab and their respective contributors is maintained indefinitely in the Git commit history, in recognition of our [commitment to contributor recognition](https://about.gitlab.com/handbook/engineering/open-source/growth-strategy.html#contributor-recognition).

However, in line with the practice of many other open source software projects, and [The Linux Foundation’s guidance](https://www.linuxfoundation.org/blog/blog/copyright-notices-in-open-source-software-projects), GitLab asks that contributor copyright notices are not added to contributions. To date there are approximately 3611 unique contributors to the GitLab project - maintaining copyright notices for all these contributors places a significant burden on maintainers and downstream distributors for little tangible benefit, and in almost all countries a copyright notice is not required for a contributor to retain ownership of their contribution.

#### Do I retain rights in my contributions?

Yes. You retain all rights in, and remain the owner of, your contributions. By accepting the terms of the GitLab CLA or DCO, you are granting GitLab broad permissions to use your contribution, but doing so does not undermine your right to use your contributions for any other purpose.

#### Can I withdraw permission to use my contributions at a later date?

No. The permissions you grant to GitLab are perpetual and cannot be withdrawn.

#### What is the difference between the Corporate CLA and the Individual CLA?

The Individual CLA is used when a contribution is made by an individual person, on their own behalf. The Corporate CLA is used when a contribution is made on behalf of an organization. If you are in doubt of which CLA is appropriate for a contribution, we recommend discussing this with your employer.

#### How can a Corporate CLA signatory manage their authorized contributors?

During preparation of a Corporate CLA, GitLab will create a group for the organization on gitlab.com under https://gitlab.com/gitlab-corporate-cla. This group will be used to manage authorized contributors on behalf of the organization. A designated number of individuals from the organization will be able to manage members of their group. This group will not be public, and as such will only be visible to members of the group.

#### Who are the responsible teams at GitLab for this process?

The Legal and Corporate Affairs team (behind `cla_managers@gitlab.com`) remain the DRI for this process. They are supported as necessary by the [Contributor Success Team](/handbook/marketing/developer-relations/contributor-success/).
